============
Introduction
============

The aim of this project is to build an MVC framework.  The MVC framework has been named as 'HotWire'.  Greatly inspired by Symfony2 full-stack Framework, so HotWire is also a full-stack framework and has an MVC framework.

HotWire
-------

HotWire has been broken down into different modules, making each one almost stand-alone and all together makes the full-stack framework.