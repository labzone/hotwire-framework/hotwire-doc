.. HotWire documentation master file, created by
   sphinx-quickstart on Wed Nov 19 21:10:47 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HotWire's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 3

   Introduction
   Overview
   modules/Framework
   modules/Http
   modules/Util
   modules/Routing
   modules/ORM
   modules/RedPlates
   modules/Form
   modules/DependencyInjection
   modules/EventDispatcher
   modules/Mailer

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

