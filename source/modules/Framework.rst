.. _framework:

=========
Framework
=========

The Framework module has the following roles:

* Main role for MVC framework
* Handle request
* Controller resolver
* Handle error

Class Diagrams
==============

MVC
---

.. figure:: /_static/mvc.png
	:alt: MVC

	Generic MVC diagram

.. include:: Framework/Controller.rst

Template View
-------------

.. figure:: /_static/TemplateView.png
	:alt: Template View

	Renders model information in HTML by passing through a templating engine.

Framework
---------

.. figure:: /_static/KernelAndApps.png
	:alt: Framework

	As the `AppKernel` get instantiated, the different routes are registered.

	Once the `AppKernel` get a request, the request is delegated to `App` from framework modules, to handle it.  An instance of `Framework` class is then created to match the request with a controller.  If a controller is not found in the application, the ErrorController controller takes over.

Kernel Template Method
----------------------

.. figure:: /_static/KernelTemplateMethod.png
	:alt: MVC

	When kernel is booted, it calls `getApps` to boot the different registered modules.