.. _redplates:

=========
RedPlates
=========

RedPlates is the module dealing with templating.  It has been built to deal with two type of templating engines, namely Twig and Plates.  Internally the framework makes use of Twig templating engine to render form, error pages, etc.  But the end-user application can specify which one of the two engines to use.

Class Diagram
=============

Strategy
--------

.. figure:: /_static/TemplatingStrategy.png
	:alt: Templating Strategy

	Strategy pattern is used to encapsulate each templating engine and make them interchangeable.

Template Method
---------------

.. figure:: /_static/TemplatingTemplateMethod.png
	:alt: Template Method

	The template method (`__contruct()`) calls the initialize method, to initialize (register template paths, set cache path, environment mode) the template engine used


Examples
========

Config
------

use of 'plates' engine (defined in app/config/parameters.json), if it is changed to 'twig', the twig engine will be used.

.. code-block:: json
	:linenos:
	:emphasize-lines: 2

	{
		"templating": "plates",
		"database":{
			"engine"    :"mysql",
			"host"      :"127.0.0.1",
			"name"      :"hotwire",
			"username"  :"root",
			"password"  :null
		},
		"smtp":{
			"host":"127.0.0.1",
			"port":25,
			"username":"username",
			"password":"password"
		},
		"debug": true
	}

Twig / Plates in controller
---------------------------

usage in controller is the same for both 'Plates' and 'Twig'

.. code-block:: php
	:linenos:

	<?php
	return $this->render('Acme:Demo::Hello/index',[
		'name'=>$name
	]);


Templating in Twig
------------------

use of variable in twig templating engine

.. code-block:: jinja
	:linenos:

	<button type="{{ type }}">{{ value }}</button>


Templating in Plates
--------------------

use of variable in plates templating engine

.. code-block:: php
	:linenos:

	<button type="<?=$this->e($type)"><?=$this->e($value)</button>
