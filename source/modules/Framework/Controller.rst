Front Controller
----------------

.. figure:: /_static/FrontController.png
	:alt: Front Controller

	The front controller combines all requests to be handled by a single handler.  The handler will then create an appropriate command (controller) to which it will delegate the request to perform the required operation.  The handler will create the command either statically or dynamically.  Dynamically in the case of routes, controllers defined, if none of them can be created then the static one is created.

	The dynamic commands are used so that developers using the framework can add other commands to their application without altering the handler as it will not make sense for developer to edit the framework to make their application run as required.

	The static commands are used for exceptions (404, 500)

	The handler is named as "AppKernel", it has been defined in "app/AppKernel.php" and is instantiated in "web/app.php" as only the web folder will be public.

Page Controller
---------------

.. figure:: /_static/PageController.png
	:alt: Page Controller

	Page controller for handling request and return a response (normally a view).