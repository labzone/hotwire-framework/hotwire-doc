.. _form:

====
Form
====

The form module is used to generate form with different fields such as TextField, ListField, ButtonField.  The main advantage of this module is that once the generated form is submitted, the value in POST or GET request is automatically mapped to an object.  The object can be directly be persisted assuming the object is an database entity and it is valid.

Class Diagrams
==============

Composite
---------

.. figure:: /_static/Composite.png
	:alt: Composite

	Composite pattern has been used to build a tree structure of form components.

Template Method
---------------

.. figure:: /_static/FormTemplateMethod.png
	:alt: Template Method

	Template method (`render`) is used to call `build` to render the form