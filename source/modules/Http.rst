.. _http:

====
HTTP
====

The HTTP module defines a object-oriented layer for HTTP specification.

Classes:

* Request - encapsulates superglobals variables ($_GET, $_POST, $_COOKIE, $_SERVER and $_FILES), to make use of them using objects rather than the superglobals, which is safer.

* Response - used for outputing simple text or used in views.

* JsonResponse - extends Response to output object in json

Note that the HTTP module is self-contained, it can be used seperately in other projects on its own.

Class Diagrams
==============

Request
-------

.. figure:: /_static/RequestFacade.png
	:alt:  Request Facade

	Facade Pattern

	Request use facade to provide a unified interface to make use of global PHP variables such as $_POST, $_GET, $_SERVER and $_FILES.

Response
--------
.. figure:: /_static/Response.png
	:alt:  Response

	Inheritance being used to override the send method of `Response` to output Json