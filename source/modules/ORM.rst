.. _orm:

===
ORM
===

The ORM manages the all database interaction by using object, performing operation such as insert, delete, select and update.  It can also create or drop table.

An entity is simply an object representing a row in table.

Class Diagrams
==============

ORM
---

.. figure:: /_static/ORM.png
	:alt: ORM

	The big picture

	The application wll have to use only the manager to perform all database interaction.  All other objects are being used internally by the ORM.

Singleton
---------

.. figure:: /_static/DatabaseHandlerSingleton.png
	:alt: Database handler

	Database Handler use singleton as it keep a single connection is needed for database.

Data Mapper
-----------

.. figure:: /_static/DataMapper.png
	:alt: Data Mapper

	The data mapper will take an entity and performs mapping, creating an `EntityProperties` which consists of the different columns with the field name, its value and data type.  After the mapping the `DataAccess` can take the `EntityProperties` to generate the proper SQL using `SQLGenerator` which can then be used to perform SQL queries using the `DatabaseHandler`.

Identity Field
--------------

.. figure:: /_static/IdentityField.png
	:alt: Identity Field

	Keep a database ID field in the entity to maintain identity between in-memory object and a database row.  Every entity must has an property `id`.

Foreign-Key Mapping
-------------------

.. figure:: /_static/ForeignKeyMapping.png
	:alt: Foreign Key Mapping

	Maps an association betwen objects to a foreign key reference between tables.  In the diagram the two classes are found in the demo application, it represents the relationship between a user and an article.

Further Development
===================

Other patterns that can be used for other implementation:

* Association Table Mapping
* Lazy Load
* Class Table Inheritance
* Concrete Table Inheritance
* Single Table Inheritance
* Repository (has been implemented but not documented)
* Unit of Work