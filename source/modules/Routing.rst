.. _routing:

=======
Routing
=======

Routing defined the urls that the application can exposed.  The different routes can be defined `Routing\Routing.php`.

The class must implements the `getRoutes` method which must return an array of `Route` class, each one defines a unique name, the route, parameters which can be passed.

The framework will try to match a URL to a route defined in this file.

Class Diagram
=============

.. figure:: /_static/Routing.png
	:alt: Routing

	The `Routing` class is placed in user application where the interface `IRoute` is in the framework.  Every routing must implement the IRoute interface for it to register.

Example
=======

.. code-block:: php
	:linenos:

	<?php

	namespace Acme\Demo\Routing;

	use HotWire\Routing\Route;
	use HotWire\Routing\IRoute;

	class Routing implements IRoute
	{
		public static function getRoutes()
		{
			return array(
				'home'=>new Route('/',array(
					'_controller'=>'Acme\Demo\Controller\ArticleController:index'
				)),
				'article_show'=>new Route('/article/{id}',array(
					'id'=>0,
					'_controller'=>'Acme\Demo\Controller\ArticleController:show'
				)),
			);
		}
	}
