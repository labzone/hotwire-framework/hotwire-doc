.. _util:

====
Util
====

The Util module is helper module.

Class Diagram
=============

.. figure:: /_static/Iterator.png
	:alt: Iterator

	Iterator

	Provide access to the elements of ArrayList without exposing its underlying representation.