.. _dependencyInjection:

====================
Dependency Injection
====================

Dependency Injection has been implemented in all modules can be made as a service, such as mailer, templating, entity manager and so on.  With dependency injection any kind of service can be injected anywhere without affecting existing code.

Any modules, that require the need to register a service, need to have an extension.  For example for the mailer module, the extension would be MailerExtension class which should have a load method in which the service would be register.

Note that the dependency injection module is self-contained, it can be used seperately in other projects on its own.

Class Diagrams
==============

Factory
-------
**Class Diagram**

.. figure:: /_static/ExtensionFactory.png
	:alt: Extension Factory

	The factory method is used internally when the framework starts, that is when AppKernel is instantiated.  Creating the services in a container to use whenever needed.  The different extension are found in their own module respectively, for example `MailerExtension` is found Mailer module.

Singleton
---------

**Class Diagram**

.. figure:: /_static/ExtensionSingleton.png
	:alt: Extension Singleton

	The extension factory has been made singleton as there's need for one instance of it to create any kind of extension.

Example
=======

The following codes should the registration of `manager` and `SQLMapper` in the container.

.. code-block:: php
	:linenos:

	<?php

	namespace HotWire\ORM\DependencyInjection;

	use HotWire\DependencyInjection\Extension;
	use HotWire\ORM\Manager;
	use HotWire\ORM\SQLMapper;

	class ORMExtension extends Extension
	{
		public function load()
		{
			$this->container->register('entity.manager', new Manager())
							->register('sql.mapper', new SQLMapper());
		}
	}


Using Manager with dependency injection in controller

.. code-block:: php
	:linenos:

	<?php

	class ArticleController extends Controller
	{

		public function __construct(Manager $manager)
		{
			$this->manager=$manager; // instance of `Manager`
		}
	.
	.
	.

Using Manager with dependency injection in any class in the project

.. code-block:: php
	:linenos:

	<?php

	$manager=Container::getInstance()->get('entity.manager'); // instance of `Manager`
	.
	.
	.