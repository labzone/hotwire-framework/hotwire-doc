.. _eventDispatcher:

================
Event Dispatcher
================

Event dispatcher is used to dispatch event to different subscriber.

A good example is when submitting a form, the data need to be validated before persisted, one solution can be valid the data in the controller itself, but this can lead to duplication of code.  Another solution is to dispatch the data to a validationSubscriber which will deal with it.

Class Diagram
=============

Observer with Mediator
----------------------

.. figure:: /_static/Observer-many-to-many.png
	:alt: Observer with mediator

	`Event` is the mediator between the subsriber and the listeners.