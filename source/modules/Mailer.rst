.. _mailer:

======
Mailer
======

Mailer is a helper module used for sending email.  The mailer make use of SwiftMailer library.  The framework is not dependent upon this module, it can be remove without any issue assuming it has not been used with in the application.


Usage:

.. code-block:: php
	:linenos:
	:emphasize-lines: 3,16

	<?php
	// Create the message
	$message = $this->get('email.message')
	    // Give the message a subject
	    ->setSubject('Your subject')

	    // Set the From address with an associative array
	    ->setFrom(array('john@doe.com' => 'John Doe'))

	    // Set the To addresses with an associative array
	    ->setTo(array('receiver@domain.org', 'other@domain.org' => 'A name'))

	    // Give it a body
	    ->setBody('Here is the message itself');

	$this->get('mailer')->send($message);
