========
Overview
========

Framework Structure
===================

.. figure:: /_static/Overview.png
	:alt: Framework

Modules
-------

* :ref:`framework`
* :ref:`util`
* :ref:`http`
* :ref:`eventDispatcher`
* :ref:`dependencyInjection`
* :ref:`routing`
* :ref:`redPlates`
* :ref:`orm`
* :ref:`form`
* :ref:`mailer`

Directory Structure
===================

app/ - keep application configuration, cache, base view template
	cache/
		- stores cache files for twig templating engine
	config/
		- keeps application configuration
	Resources/
		- keeps base view template where application views can extends from
	AppKernel.php
		- Application kernel, start application
doc/
	- this documentation
src/
	- application source code
vendor/
	- holds the framework and dependencies
composer.json
	- file to download dependencies with composer
