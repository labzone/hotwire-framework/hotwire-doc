<?php

use Sami\Sami;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('Resources')
    ->exclude('Tests')
    ->in('../../src')
;

return new Sami($iterator,array(
	'title'				   => 'HotWire API',
	'build_dir'            => __DIR__.'/../build/html/api',
	'default_opened_level' => 5,
));